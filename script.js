// Assign Elements
const bars = document.querySelectorAll(".bar");
const btn = document.querySelector("#randomize-btn");


// Function
function randomize() {
    function generateColor() {
        let color = "#";

        for(let i = 0; i < 6; i++) {
            color += (Math.floor(Math.random()*16)).toString(16);
        }

        return color;
    }

    bars.forEach(bar => {
        hexCode = generateColor();
        bar.style.backgroundColor = hexCode;
        bar.innerHTML = hexCode;
    })
}


// Assign Event Listener
btn.addEventListener("click", randomize, false);

randomize();